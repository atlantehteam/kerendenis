<?php
// Add custom Theme Functions here
add_filter('wpcf7_spam', '__return_false');

function child_enqueue_scripts() {
	wp_enqueue_style(
		'flatsome-child',
		get_stylesheet_directory_uri() . '/style.css', [], filemtime( get_stylesheet_directory() . '/style.css' )
	);
}
add_action( 'wp_enqueue_scripts', 'child_enqueue_scripts' );




function roi_debug($log) {
	$logLine = date('d-m-y H:i:s').': '.$log.PHP_EOL;
	file_put_contents(WP_CONTENT_DIR.'/wip.log', $logLine, FILE_APPEND);
}


add_filter('advanced_woo_discount_rules_get_regular_price', 'ks_filter_regular_price', 10, 2);
function ks_filter_regular_price($price, $product) {
	foreach (wc()->cart->get_cart() as $key => $cart_item) {
		if (!empty($cart_item['ccb_calculator']) && $cart_item['ccb_calculator']['product_id'] == $product->get_id()) {
			return $cart_item['ccb_calculator']['ccb_total'];
		}
	}
	return $price;
}


// add_action('woocommerce_before_calculate_totals', 'ks_adjust_totals', 1000, 1);
// function ks_adjust_totals($cart) {
// 	if (empty($cart->applied_coupons) || !in_array('reut25', $cart->applied_coupons)) {
// 		return;
// 	}
// 	roi_debug('cart_total');
// 	roi_debug(json_encode($cart));
// 	foreach ($cart->cart_contents as $id => $cart_item) {
// 		if (!empty($cart_item['ccb_calculator'])) {
// 			roi_debug('calc item found');
// 			roi_debug(json_encode($cart_item));
// 			$cart_item['data']->set_price($cart_item['ccb_calculator']['ccb_total'] * 0.75);
// 			// $cart_item->set_price(10);
// 		}
// 	}
// }